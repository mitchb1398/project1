"""
Author: Mitchell Bratina
"""
import sys
from typing import List

class Task:
    def __init__(self, name : str, readyTime : int, compTime : int, deadline : int, resource : int =-1, resourceAccess : str = 'none'):
        self.name = name
        self.readyTime = readyTime
        self.compTime = compTime
        self.deadline = deadline
        self.resource = resource
        self.resourceAccess = resourceAccess

    def get_name(self):
        return self.name

    def get_readyTime(self):
        return self.readyTime

    def get_compTime(self):
        return self.compTime

    def get_deadline(self):
        return self.deadline

    def get_resource(self):
        return self.resource

    def get_access(self):
        return self.resourceAccess

    def get_EST(self, processorList : List, resourceList : List):
        # Get smallest processor ready time
        readyTimes = []
        for processor in processorList:
            readyTimes.append(processor.next_ready())

        nextProcessor = min(readyTimes)

        # Get next resource ready
        if self.resource != -1:
            nextResource = resourceList[self.resource].next_available(self)
        else:
            nextResource = 0

        return max(nextProcessor, nextResource, self.readyTime)

    def feasible(self, processorList : List, resourceList : List):
        if self.get_EST(processorList, resourceList) + self.compTime <= self.deadline:
            return True
        return False

class Resource:
    def __init__(self):
        self.currentUse = 'free'
        self.available = 0

    def next_available(self, task : Task):
        """Return when resource is available for accessType
        """
        accessType = task.get_access()

        if self.currentUse == 'free':
            return 0
        
        elif self.currentUse == 'shared':
            if accessType == 'shared':
                return 0
            elif accessType == 'exclusive':
                return self.available
            
        elif self.currentUse == 'exclusive':
            return self.available

        sys.exit("Resource availablility check failed: reason unknown.")

    def use(self, task : Task, estT):
        """Use resource and update information for next node/system state
        """
        # accessType = task.get_access()
        # accessTime = task.get_compTime()
        # taskStart = task.get_EST()

        # if self.currentUse == 'free':
        #     self.currentUse = accessType
        #     self.available = accessTime
        #     return
        
        # elif self.currentUse == 'shared':
        #     if accessType == 'shared':
        #         self.available = max(self.available, taskStart + accessTime)
        #         return
        #     elif accessType == 'exclusive':
        #         self.available = taskStart + accessTime
        #         self.currentUse = accessType
            
        # elif self.currentUse == 'exclusive':
        #     self.available = self.available + accessTime
        #     self.currentUse = accessType

        # sys.exit("Resource use failed: reason unknown.")

        accessType = task.get_access()
        accessTime = task.get_compTime()
        taskStart = estT

        self.available = taskStart + accessTime
        self.currentUse = accessType


class Processor:
    def __init__(self, name):
        self.ready = 0
        self.name = name

    def next_ready(self):
        return self.ready

    def use(self, task : Task, estT):
        self.ready =  estT + task.get_compTime()

    def get_name(self):
        return self.name
