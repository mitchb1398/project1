"""
Author: Mitchell Bratina
"""
from os import wait
from pickle import STOP
from system import Resource, Processor, Task
from node import Node
import pickle
import sys
from copy import deepcopy, copy

NumberOfBacktracks = 0


def main(repeatLastRun : bool =True):

    if (not repeatLastRun):
        # Get info from user and set up variables
        numProcessors = int(input("Enter the number of processors for the system: "))
        #processors = [Processor()] * numProcessors
        processors = []
        for index in range(numProcessors):
            processors.append(Processor(f"P{index + 1}"))


        numResources = int(input("Enter the number of resources for the system: "))
        #resources = [Resource()] * numResources
        resources = []
        for _ in range(numResources):
            resources.append(Resource())

        windowSize = int(input("Enter the size of the check window: "))

        maxBacktracks = int(input("Enter the number of maximum backtracks: "))

        W = int(input("In the heuristic function 'Hi = di + (W * EST(Ti))', what would you like W to be? (integer): "))

        # Get info about tasks
        tasks = []
        doneWithTasks = False
        taskNumber = 0
        while not doneWithTasks:
            isResource = "x"
            taskResource = 0
            taskAccess = "none"
            taskNumber += 1

            taskReady = int(input(f"Enter the ready time for task no.{taskNumber}: "))
            taskComp = int(input(f"Enter the completion time for task no.{taskNumber}: "))
            taskDead = int(input(f"Enter the deadline for task no.{taskNumber}: "))
            while ((isResource != "y") and (isResource != "n")):
                isResource = input(f"Does task no.{taskNumber} use a resource? y/n: ").lower()

            if (isResource == "y"):
                while ((taskResource < 1) or (taskResource > numResources)):
                    taskResource = int(input(f"Enter resource task no.{taskNumber} uses (as integer >0): "))
                    if ((taskResource < 1) or (taskResource > numResources)):
                        print("Invalid resource")
                
                while ((taskAccess != "shared") and (taskAccess != "exclusive")):
                    taskAccess = input(f"Enter resource access for task no.{taskNumber} ('shared' or 'exclusive'): ")
                    if ((taskAccess != "shared") and (taskAccess != "exclusive")):
                        print("Invalid access type")

            taskName = f"T{taskNumber}"

            newTask = Task(taskName, taskReady, taskComp, taskDead, taskResource - 1, taskAccess)
            tasks.append(newTask)

            done = 'x'
            while ((done != 'y') and (done != 'n')):
                done = input("Do you have more tasks? (y/n): ").lower()
                if ((done != 'y') and (done != 'n')):
                    print("Invalid input")

            if (done == 'n'):
                doneWithTasks = True

        saveInfo = {
            "processors" : processors,
            "resources" : resources,
            "windowSize" : windowSize,
            "maxBacktracks" : maxBacktracks,
            "W" : W,
            "tasks" : tasks
        }
        with open("./lastRun.sysInfo",'wb') as saveFile:
            pickle.dump(saveInfo, saveFile)

    else:
        with open("./lastRun.sysInfo", 'rb') as loadFile:
            loadInfo = pickle.load(loadFile)

        processors = loadInfo.get("processors")
        resources = loadInfo.get("resources")
        windowSize = loadInfo.get("windowSize")
        maxBacktracks = loadInfo.get("maxBacktracks")
        W = loadInfo.get("W")
        tasks = loadInfo.get("tasks")



    # Create root node
    rootNode = Node(None, None, None, windowSize, tasks, processors, resources, W, True)

    recursive_find(rootNode, maxBacktracks)



def recursive_find(node : Node, maxBacktracks : int, debug : bool =True):
    global NumberOfBacktracks
    if debug: print("Checkpoint: 1")

    # Check if only one task left and if feasible return successful schedule
    if len(node.get_taskList()) == 1:
        if debug: print("One left")
        finalTask = node.get_taskList()[0]
        if finalTask.feasible(node.get_processorList(), node.get_resourceList()):
            print("Successful Schedule Found!")

            # Gather Schedule
            scheduleList = []
            processorList = []
            scheduleList.append(finalTask)
            processorList.append(node.next_ready_processor())
            scheduleList.append(node.get_task())
            processorList.append(node.get_processor())
            while node.get_parent() is not None:
                scheduleList.append(node.get_parent().get_task())
                processorList.append(node.get_parent().get_processor())
                node = node.get_parent()

            scheduleList.reverse()
            scheduleList.pop(0)
            processorList.reverse()
            processorList.pop(0)

            print("Schedule:")
            for index in range(len(scheduleList)):
                print(f"Task {scheduleList[index].get_name()} on Processor {processorList[index].get_name()}")
            print(f"Total number of backtracks: {NumberOfBacktracks}")
            sys.exit()
        else:
            NumberOfBacktracks += 1
            return 0
            

    # Check the strong feasibility of the check window
    for index in range(min(node.get_windowSize(), len(node.get_taskList()))):
        tempTask = node.get_taskList()[index]
        if debug: print(f"tempTask is task {tempTask.get_name()}")
        if debug: 
            print(f"EST({tempTask.get_name()}) = {tempTask.get_EST(node.get_processorList(), node.get_resourceList())}")
            print("Processor ready times:")
            for processor in node.get_processorList():
                print(processor.next_ready())
            if tempTask.get_resource() != -1:
                print("Resource ready times:")
                for resource in node.get_resourceList():
                    print(resource.next_available(tempTask))
                    
        # If any of the tasks are not feasible,
        if (not tempTask.feasible(node.get_processorList(), node.get_resourceList())):
            # Return to the parent node and reporting a backtrack of 1
            if debug: 
                print(f"Task {tempTask.get_name()} is not feasible.")
                print(f"EST: {tempTask.get_EST(node.get_processorList(), node.get_resourceList())}")
                print(f"Comptime: {tempTask.get_compTime()}")
                print(f"Deadline: {tempTask.get_deadline()}")
                print("Processor ready times:")
                for processor in node.get_processorList():
                    print(processor.next_ready())
                print("Backtrack!")
            
            return 1

    if debug: print("All feasible")

    # Find the heuristic scores of the tasks in the check window
    heuristicScores = []
    for index in range(min(node.get_windowSize(), len(node.get_taskList()))):
        taskT = node.get_taskList()[index]
        hT = heuristic_func(taskT.get_EST(node.get_processorList(), node.get_resourceList()), taskT.get_deadline(), node.get_W())
        heuristicScores.append(hT)
    if debug: print(heuristicScores)

    # Create list window tasks sorted by heuristic scores
    windowTasks = list(node.get_taskList()[0:node.get_windowSize()])
    sortedWindowTasks = [task for score,task in sorted(zip(heuristicScores,windowTasks), key=lambda pair: pair[0])]
    if debug:
        for task in sortedWindowTasks: print(task.get_name())

    # Set up environment for child nodes and run recursive on them
    processorToUseIndex = node.get_processorList().index(node.next_ready_processor())
    
    results = []
    for task in sortedWindowTasks:

        processorListTemp = deepcopy(node.get_processorList())
        resourceListTemp = deepcopy(node.get_resourceList())

        childNode = Node(node, task, processorListTemp[processorToUseIndex], node.get_windowSize(), node.get_taskList(), processorListTemp, resourceListTemp, node.get_W())
        childNode.pop_task(childNode.get_task())

        if debug:
            names = []
            for index in range(min(childNode.get_windowSize(), len(childNode.get_taskList()))):
                names.append(childNode.get_taskList()[index].get_name())
            print(f"Choose {childNode.get_task().get_name()}, next window: {names}")

        tempEST = childNode.get_task().get_EST(childNode.get_processorList(), childNode.get_resourceList())

        # Update processor for child node
        if debug: 
            print("Updating processor")
            print(f"Before: {childNode.get_processor().next_ready()}")
            
        childNode.use_processor(childNode.get_processor(), childNode.get_task(), tempEST)

        if debug: 
            print(f"After: {childNode.get_processor().next_ready()}")
            for processor in childNode.get_processorList():
                print(processor.next_ready())


        # Update resource for child node
        if childNode.get_task().get_resource() != -1:
            if debug:
                print("Updating resource")
                print(f"Before: {childNode.get_resourceList()[childNode.get_task().get_resource()].next_available(childNode.get_task())}")
                print(f"EST({childNode.get_task().get_name()}) = {tempEST}")
                print(f"Runtime = {childNode.get_task().get_compTime()}")
            
            childNode.use_resource(childNode.get_task().get_resource(), childNode.get_task(), tempEST)

        # Run recursive_find on child node
        if debug: print(f"Recursion with task {task.get_name()}")
        result = recursive_find(childNode, maxBacktracks, debug)
        if NumberOfBacktracks > maxBacktracks:
            print("MAX Backtracks Reached: unable to find schedule.")
            sys.exit()
        results.append(result)
        if debug: print(results)

    if node.parent == None:
        print("No more backtracks possible: unable to find schedule.")
    return sum(results)
    


def heuristic_func(estT : int, dT : int, W : int =0):
    hT = dT + (W * estT)
    return hT


if __name__ == "__main__":
    main()
