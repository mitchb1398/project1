"""
Author: Mitchell Bratina
"""

from system import Task

class Node:

    def __init__(self, parent, task : Task, processor : int, windowSize : int =3, taskList : list =[], processorList : list =[], resourceList : list =[], W : int =0, rootNode : bool =False):
        self.parent = parent
        self.task = task
        self.processor = processor
        self.taskList = list(taskList)
        self.processorList = list(processorList)
        self.resourceList = list(resourceList)
        self.windowSize = windowSize
        self.W = W
      
            
    def get_parent(self):
        return self.parent

    def get_task(self):
        return self.task

    def get_taskList(self):
        return self.taskList

    def pop_task(self, task):

        self.taskList.pop(self.taskList.index(task))
        return

    def get_processorList(self):
        return self.processorList

    def get_resourceList(self):
        return self.resourceList

    def get_processor(self):
        return self.processor

    def get_windowSize(self):
        return self.windowSize

    def use_resource(self, resource : int, task, estT):
        self.resourceList[resource].use(task, estT)

    def use_processor(self, processor, task, estT):
        processor.use(task, estT)

    def next_ready_processor(self):
        processorToUse = self.processorList[0]
        for processor in self.processorList:
            if processor.next_ready() < processorToUse.next_ready():
                processorToUse = processor

        return processorToUse

    def get_W(self):
        return self.W




        